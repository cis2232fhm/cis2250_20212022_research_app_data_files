package info.hccis.performancehall_mobileappbasicactivity.bo;

import androidx.lifecycle.ViewModel;

import java.util.ArrayList;

public class TicketOrderViewModel extends ViewModel {

    private ArrayList<TicketOrderBO> ticketOrders = new ArrayList();

    public ArrayList<TicketOrderBO> getTicketOrders() {
        return ticketOrders;
    }

    public void setTicketOrders(ArrayList<TicketOrderBO> ticketOrders) {
        this.ticketOrders = ticketOrders;
    }
}
