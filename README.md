# App data and files
***
This application uses the existing performance hall application, and adds a counter of the total calculations there have been. It uses a file to keep track of the total calculations. You'll likely need to trust the project to build it.
***
## Known issues:
* Is built off of the wrong/old code base (https://bitbucket.org/cisbjmaclean/cis2250_20212022_research_getting_started/ commit ad93c038c3c322a49b2c050b74c3f5365bba87b4)
***
## TODOs / Would be nice to have:
* Only flush when the application is closed (to avoid writes).