package info.hccis.performancehall_mobileappbasicactivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import info.hccis.performancehall_mobileappbasicactivity.bo.TicketOrderBO;
import info.hccis.performancehall_mobileappbasicactivity.bo.TicketOrderViewModel;
import info.hccis.performancehall_mobileappbasicactivity.databinding.FragmentAddOrderBinding;

public class AddOrderFragment extends Fragment {

	public static final String KEY = "info.hccis.performancehall.ORDER";
	private FragmentAddOrderBinding binding;
	TicketOrderBO ticketOrderBO;
	TicketOrderViewModel ticketOrderViewModel;

	//The file's name for the total calculations counter (only used if USE_KEY_VALUE = false).
	final String FILE_NAME = "totalCalculations",
	//The preference's name for the total calculations counter (only used if USE_KEY_VALUE = true).
	PREFERENCE_NAME = "TOTAL_CALCULATIONS";
	//The file itself. We need it to be accessible everywhere (only used if USE_KEY_VALUE = false).
	File file;
	/*Boolean which decides if the counter components function or not. Is set to true whenever an
	error occurs, and  is used to stop operations which can cause the app to no longer be able to do
	calculations.*/
	boolean noCounter = false;
	//The preference for the total calculations counter (only used if USE_KEY_VALUE = true).
	SharedPreferences sharedPreferences;

	//TODO:CHANGE ME! If USE_KEY_VALUE = false, we use a file to store our number. Otherwise, we use a SharedPreferences.
	final boolean USE_KEY_VALUE = false;

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d("AddOrderFragment BJM", "onCreate triggered");
		if (USE_KEY_VALUE) {
			sharedPreferences = getActivity().getPreferences(Context.MODE_PRIVATE);
			return;
		}
		file = new File(getContext().getFilesDir(), FILE_NAME);
	}

	@Override
	public View onCreateView(
			LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState
	) {

		Log.d("AddOrderFragment BJM", "onCreateView triggered");
		binding = FragmentAddOrderBinding.inflate(inflater, container, false);
		return binding.getRoot();

	}


	public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		Log.d("AddOrderFragment BJM", "onViewCreated triggered");

		//Update the total calculations counter (provided noCounter = false).
		updateCounter();

		ticketOrderViewModel = new ViewModelProvider(getActivity()).get(TicketOrderViewModel.class);

		binding.buttonCalculate.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				Log.d("AddOrderFragment BJM", "Calculate was clicked");

				try {
					calculate();
					Bundle bundle = new Bundle();
					bundle.putSerializable(KEY, ticketOrderBO);

					if (USE_KEY_VALUE) {
                        /*Commit (AKA flush (in short terms, ensure the write we just did does not
                        stay cached in memory, and has actually been written to the "disk".)) to
                        the "disk" right now, instead of whenever the device decides to do so (so
                        that we are sure the data is there). This may freeze the UI if the "disk" is
                        particularly slow. NOTE: Flushing data is generally not a good thing to do
                        (excessive flushing can cause a reduced lifespan in flash memory, and can
                        also cause excessive I/O operations for no good reason. If we did not flush,
                        the data would have remained in memory. If we do multiple calculations, this
                        is actually beneficial, as it is quicker to access something in memory than
                        accessing something on flash memory (most of the time). It also avoids the
                        unnecessary writes. However, this does bring the downside of the device
                        choosing when to write the data to the flash memory. Instead, maybe consider
                        flushing when the application closes (I couldn't find any information on how
                        Android decides when or what to flush stuff to the disk), but for this short
                        sample application, this is OK.).*/
						sharedPreferences.edit().putLong(PREFERENCE_NAME, sharedPreferences.getLong(PREFERENCE_NAME, 0) + 1).commit();
					} else {
						long totalCalculations = Long.parseLong(readTotalCalculationsFile().toString());
						/*We open a FileOutputStream. This automatically truncates (empties) the
						file, unless a boolean (which is set to true) is added after the file. For
						our case, we want the file truncated.*/
						FileOutputStream fileOutputStream = new FileOutputStream(file);
						/*We then write the string of numbers to the file. We use .getBytes() to
						convert the string to a char array (for example, ABC becomes A,B,C...)*/
						fileOutputStream.write(String.valueOf(++totalCalculations).getBytes());
						//Flush to ensure our data is there. See the note above about using flush.
						fileOutputStream.flush();
						//Close the FileOutputStream as we are done writing.
						fileOutputStream.close();
					}

					updateCounter();

					NavHostFragment.findNavController(AddOrderFragment.this)
							.navigate(R.id.action_AddOrderFragment_to_ViewOrdersFragment, bundle);
				} catch (Exception e) {
					e.printStackTrace();
					Log.d("AddOrderFragment BJM", "Error calculating: " + e.getMessage());

				}
			}
		});

	}

	/**
	 * calculate the ticket cost based on the controls on the view.
	 *
	 * @throws Exception Throw exception if number of tickets entered caused an issue.
	 * @author CIS2250
	 * @since 20220118
	 */
	public void calculate() throws Exception {

		Log.d("BJM-MainActivity", "HollPass Number entered =" + binding.editTextHollpassNumber.getText().toString());
		Log.d("BJM-MainActivity", "Number of tickets = " + binding.editTextNumberOfTickets.getText().toString());
		Log.d("BJM-MainActivity", "Calculate button was clicked.");

		int hollPassNumber = 0;
		try {
			hollPassNumber = Integer.parseInt(binding.editTextHollpassNumber.getText().toString());
		} catch (Exception e) {
			hollPassNumber = 0;
		}
		boolean validHollPassNumber;
		int numberOfTickets;
		try {
			numberOfTickets = Integer.parseInt(binding.editTextNumberOfTickets.getText().toString());
		} catch (Exception e) {
			numberOfTickets = 0;
		}
		ticketOrderBO = new TicketOrderBO(hollPassNumber, numberOfTickets);

		try {
			if (!ticketOrderBO.validateNumberOfTickets()) {
				throw new Exception("Invalid Number of tickets");
			}
			double cost = ticketOrderBO.calculateTicketPrice();

			ticketOrderViewModel.getTicketOrders().add(ticketOrderBO);

			//Now that I have the cost, want to set the value on the textview.
			Locale locale = new Locale("en", "CA");
			DecimalFormat decimalFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(locale);
			DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance(locale);
			decimalFormat.setDecimalFormatSymbols(dfs);
			String formattedCost = decimalFormat.format(cost);
			binding.textViewCost.setText(formattedCost);

		} catch (NumberFormatException nfe) {
			binding.editTextNumberOfTickets.setText("");
			binding.textViewCost.setText("Invalid number of tickets");
			throw nfe;
		} catch (Exception e) {
			binding.editTextNumberOfTickets.setText("");
			binding.textViewCost.setText("Maximum number of tickets is " + TicketOrderBO.MAX_TICKETS);
			throw e;
		}
	}

	/**
	 * Gets the total amount of calculations (as a string).
	 *
	 * @author fhm
	 * @since 20220130
	 */
	public StringBuilder readTotalCalculationsFile() {
		//Value holding the current character that was just read.
		int data;
        /*StringBuilder which (if conditions prove right) returns the current number of total
        calculations.*/
		StringBuilder numbers = new StringBuilder();
        /*This part verifies the existence of a file. If the file does not exist, we return a
        StringBuilder with a 0.*/
		if (file.exists()) {
            /*If for some reason the file is a directory, we do not want to proceed any further,
            as we cannot write to a directory. Additionally, we should not try to delete this
            folder, since we do not know where it came from, and we also do not know if there is
            anything of importance in it.*/
			if (file.isDirectory()) {
				disableCounter("Failed to read file, file is a directory.");
                /*If for some reason the file does exist, but it is empty, we can still proceed, but
                we should throw a warning about this, as this should never occur.*/
			} else if (file.length() == 0) {
				numbers.append(0);
				Log.w("readTotalCalculationsFile", "File exists, but is empty. Returning 0.");
				//The file exists, isn't a directory, nor is it empty. Good. Let's try to read it.
			} else {
				/*If for whatever reason we get an IOException, we disable the counter, as this
                should not occur.*/
				try {
					//We open a FileInputStream.
					FileInputStream fileInputStream = new FileInputStream(file);
                    /*While the character we get isn't -1 (EOF, or end of file), we append the
                    number we get to the StringBuilder. We need to offset by 48 otherwise we are
                    returning the ASCII value of the character, and not the actual value (0).*/
					while ((data = fileInputStream.read()) != -1) {
						numbers.append(data - 48);
					}
					//Close the FileInputStream as we are done reading.
					fileInputStream.close();
				} catch (IOException e) {
					disableCounter("Failed to read file.");
				}
			}
		} else {
			numbers.append(0);
			Log.i("readTotalCalculationsFile", "File doesn't exist, returning 0.");
		}
		return numbers;
	}

	/**
	 * Updates the total calculations counter.
	 *
	 * @author fhm
	 * @since 20220130
	 */
	public void updateCounter() {
		if (USE_KEY_VALUE) {
			((EditText) getView().findViewById(R.id.editTextTotalCalculations)).setText(String.valueOf(sharedPreferences.getLong(PREFERENCE_NAME, 0)));
			return;
		}
		((EditText) getView().findViewById(R.id.editTextTotalCalculations)).setText(readTotalCalculationsFile().toString());
	}

	/**
	 * Disables the total calculations counter.
	 *
	 * @author fhm
	 * @since 20220130
	 */
	public void disableCounter(String error) {
		noCounter = true;
		((EditText) getView().findViewById(R.id.editTextTotalCalculations)).setVisibility(View.GONE);
		((TextView) getView().findViewById(R.id.textViewTotalCalculations)).setVisibility(View.GONE);
		Toast.makeText(getContext(), "Cannot load the total calculations.", Toast.LENGTH_SHORT).show();
		Log.e("disableCounter",error);
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		binding = null;
	}

}